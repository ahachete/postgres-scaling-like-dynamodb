/*
 * Copyright (C) 2021 OnGres, Inc.
 * SPDX-License-Identifier: GPL-3.0-only
 */


package com.ongres.labs.pglikedy.common.config;


import io.quarkus.arc.config.ConfigProperties;

import java.util.ArrayList;
import java.util.List;


@ConfigProperties(prefix = "benchController", namingStrategy = ConfigProperties.NamingStrategy.VERBATIM)
public class ShardsConfiguration {
    private List<String> shards;
    private String user;
    private String password;
    private String database;

    public List<String> getShards() {
        return shards;
    }

    public void setShards(List<String> shards) {
        this.shards = shards;
    }

    public void addShard(String shard) {
        if(null == shards) {
            synchronized (this) {
                if(null == shards) {
                    shards = new ArrayList<>();
                }
            }
        }

        shards.add(shard);
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }
}
