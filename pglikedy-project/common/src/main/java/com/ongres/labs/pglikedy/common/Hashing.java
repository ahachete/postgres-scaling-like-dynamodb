/*
 * Copyright (C) 2021 OnGres, Inc.
 * SPDX-License-Identifier: GPL-3.0-only
 */


package com.ongres.labs.pglikedy.common;


import net.openhft.hashing.LongHashFunction;

import java.nio.charset.StandardCharsets;


public class Hashing {
    private static final long HASH_SEED = 0x1b10e430cd78985bL;      // got it from `dd if=/dev/urandom bs=1 count=8`

    private static final LongHashFunction HASH_FUNCTION = LongHashFunction.xx(HASH_SEED);

    public static long hashString(String value) {
        return HASH_FUNCTION.hashBytes(
                value.getBytes(StandardCharsets.UTF_8)
        );
    }
}
