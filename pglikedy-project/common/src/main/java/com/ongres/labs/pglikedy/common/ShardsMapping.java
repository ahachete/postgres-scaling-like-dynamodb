/*
 * Copyright (C) 2021 OnGres, Inc.
 * SPDX-License-Identifier: GPL-3.0-only
 */


package com.ongres.labs.pglikedy.common;


import com.ongres.labs.pglikedy.common.config.ShardsConfiguration;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;


@Singleton
public class ShardsMapping {
    private int numberShards;
    private long chunkSize;
    private String[] shards;

    @Inject
    ShardsConfiguration shardsConfiguration;

    @PostConstruct
    void postConstruct() {
        var shardList = shardsConfiguration.getShards();
        this.numberShards = shardList.size();
        assert numberShards > 1;

        chunkSize = (long) Math.floor(
                ((double) Long.MAX_VALUE - Long.MIN_VALUE) / numberShards
        );

        shards = shardList.stream()
                .sorted()
                .toArray(String[]::new);
    }

    public String hashToShard(long hash) {
        return shards[(int) hash % numberShards];
    }

    public String stringToShard(String value) {
        return hashToShard(Hashing.hashString(value));
    }

    public Map<String,Integer> shardsPositions() {
        var counter = new AtomicInteger(0);

        return Arrays.stream(shards)
                .collect(
                        Collectors.toUnmodifiableMap(Function.identity(), s -> counter.getAndIncrement())
                );
    }
}
