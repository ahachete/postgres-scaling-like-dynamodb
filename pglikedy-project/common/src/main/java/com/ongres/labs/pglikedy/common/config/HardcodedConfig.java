/*
 * Copyright (C) 2021 OnGres, Inc.
 * SPDX-License-Identifier: GPL-3.0-only
 */


package com.ongres.labs.pglikedy.common.config;


public interface HardcodedConfig {
    String USERNAME = "pglikedy";
    String PASSWORD = "PgLik3Dy";
    String DATABASE = USERNAME;
    String BASE_TABLE_NAME = USERNAME;
    int POSTGRES_PORT = 5432;
}
