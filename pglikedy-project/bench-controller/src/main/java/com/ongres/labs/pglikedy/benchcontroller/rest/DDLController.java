/*
 * Copyright (C) 2021 OnGres, Inc.
 * SPDX-License-Identifier: GPL-3.0-only
 */


package com.ongres.labs.pglikedy.benchcontroller.rest;


import com.ongres.labs.pglikedy.benchcontroller.pg.PostgresController;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/ddl")
public class DDLController {

    @Inject
    PostgresController postgresController;

    @GET
    @Path("/create")
    @Produces(MediaType.TEXT_PLAIN)
    public String create() {
        postgresController.genDDL();

        return "ok";
    }
}
