/*
 * Copyright (C) 2021 OnGres, Inc.
 * SPDX-License-Identifier: GPL-3.0-only
 */


package com.ongres.labs.pglikedy.benchcontroller.pg;


import com.ongres.labs.pglikedy.common.ShardsMapping;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.SQLException;

import static com.ongres.labs.pglikedy.common.config.HardcodedConfig.BASE_TABLE_NAME;


@ApplicationScoped
public class PostgresController {
    @Inject
    ShardsMapping shardsMapping;

    public void genDDL() {
        shardsMapping.shardsPositions().entrySet().stream().forEach(entry -> {
            var host = entry.getKey();
            var ddl = genDDLTable(entry.getValue());
            try {
                SqlHelper.executeDdl(ddl, SqlHelper.getConnectionString(host));
            } catch (SQLException throwables) {
                throw new RuntimeException("Error when creating DDL", throwables);
            }
        });
    }

    private String genDDLTable(int shardNumber) {
        var ddlBuilder = new StringBuilder();
        ddlBuilder
                .append("CREATE TABLE").append(" ")
                .append(BASE_TABLE_NAME).append("_").append(shardNumber).append(" ")
                .append("(hash bigint, content jsonb)");

        return ddlBuilder.toString();
    }
}
