/*
 * Copyright (C) 2021 OnGres, Inc.
 * SPDX-License-Identifier: GPL-3.0-only
 */


package com.ongres.labs.pglikedy.benchcontroller.pg;


import java.sql.SQLException;


@FunctionalInterface
public interface SqlExceptionSupplier<Supplier> {
     Supplier get() throws SQLException;
}
