/*
 * Copyright (C) 2021 OnGres, Inc.
 * SPDX-License-Identifier: GPL-3.0-only
 */


package com.ongres.labs.pglikedy.benchcontroller.pg;


import com.ongres.labs.pglikedy.common.config.HardcodedConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class SqlHelper {
    private static final String JDBC = "jdbc";
    private static final String JDBC_POSTGRES = JDBC + ":" + "postgresql" + "://";

    public static String getConnectionString(String host) {
        return JDBC_POSTGRES + host + ":" + HardcodedConfig.POSTGRES_PORT + "/" + HardcodedConfig.DATABASE;
    }

    private static boolean executeUpdate(
            SqlExceptionSupplier<Connection> connectionSupplier, String query, int expectedRows
    ) throws SQLException {
        try(
                var connection = connectionSupplier.get();
                var statement = connection.createStatement()
        ) {
            return statement.executeUpdate(query) == expectedRows;
        }
    }

    public static boolean executeDdl(String ddl, String connectionString)
    throws SQLException {
        return executeUpdate(
                () -> DriverManager.getConnection(connectionString, HardcodedConfig.USERNAME, HardcodedConfig.PASSWORD),
                ddl,
                0
        );
    }
}
