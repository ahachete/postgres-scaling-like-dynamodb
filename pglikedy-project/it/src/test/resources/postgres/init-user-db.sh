#!/bin/bash

set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
	create user pglikedy password 'PgLik3Dy';
	create database pglikedy owner pglikedy;
EOSQL

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname pglikedy <<-EOSQL
  create extension pg_stat_statements;
  select pg_stat_statements_reset();
EOSQL