/*
 * Copyright (C) 2021 OnGres, Inc.
 * SPDX-License-Identifier: GPL-3.0-only
 */


package com.ongres.labs.pglikedy.it;


import com.ongres.junit.docker.*;
import com.ongres.labs.pglikedy.common.config.ShardsConfiguration;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.representer.Representer;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;

import static com.ongres.labs.pglikedy.common.config.HardcodedConfig.*;
import static com.ongres.labs.pglikedy.it.Environment.*;


@DockerExtension({
        @DockerContainer(
                extendedBy = DockerExtensionConfiguration.class
                ,alias = SHARD_1
                ,whenReuse = WhenReuse.ALWAYS
        ),
        @DockerContainer(
                extendedBy = DockerExtensionConfiguration.class
                ,alias = SHARD_2
                ,whenReuse = WhenReuse.ALWAYS
        ),
        @DockerContainer(
                extendedBy = DockerExtensionConfiguration.class
                ,alias = SHARD_3
                ,whenReuse = WhenReuse.ALWAYS
        )
})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SetupEnvironmentTest {
    private final ShardsConfiguration shardsConfiguration = new ShardsConfiguration();

    public SetupEnvironmentTest() {
        shardsConfiguration.setUser(USERNAME);
        shardsConfiguration.setPassword(PASSWORD);
        shardsConfiguration.setDatabase(DATABASE);
    }

    private void verifyDbConnection(String host) throws SQLException {
        try (
                Connection connection = DriverManager.getConnection(
                        "jdbc:postgresql://" + host + ":" + POSTGRES_PORT
                                + "/" + DATABASE,
                        USERNAME, PASSWORD
                );
                Statement s = connection.createStatement();
                ResultSet resultSet = s.executeQuery("select 42")
        ) {
            if(! resultSet.next()) {
                Assertions.fail("Ping query returned no result");
            }

            Assertions.assertEquals(42, resultSet.getInt(1));
        }
    }

    private void verifyConnectionAndSetShard(Container postgres)
    throws SQLException, InterruptedException {
        verifyDbConnection(postgres.getIp());
        shardsConfiguration.addShard(postgres.getIp());
    }

    @Test
    public void testShard1Connection(@ContainerParam(Environment.SHARD_1) Container postgres) throws Exception {
        verifyConnectionAndSetShard(postgres);
    }

    @Test
    public void testShard2Connection(@ContainerParam(Environment.SHARD_2) Container postgres) throws Exception {
        verifyConnectionAndSetShard(postgres);
    }

    @Test
    public void testShard3Connection(@ContainerParam(Environment.SHARD_3) Container postgres) throws Exception {
        verifyConnectionAndSetShard(postgres);
    }

    @AfterAll
    public void writeShardsConfig() throws IOException {
        Files.createDirectories(Paths.get(GENERATED_FILES_DIRECTORY));

        DumperOptions options = new DumperOptions();
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);

        Representer representer = new Representer();

        // This class is just a wrapper to adjust the resulting YAML representation
        // Possibly there's a better way to do this with snakeYAML, but works for now
        final class BenchController {
            ShardsConfiguration benchController;

            public ShardsConfiguration getBenchController() {
                return benchController;
            }

            public void setBenchController(ShardsConfiguration benchController) {
                this.benchController = benchController;
            }
        }

        BenchController benchController = new BenchController();
        benchController.setBenchController(shardsConfiguration);

        representer.addClassTag(BenchController.class, Tag.MAP);

        Yaml yaml = new Yaml(representer, options);

        try(FileWriter fileWriter = new FileWriter(GENERATED_SHARDS_CONFIG_FILE)) {
            yaml.dump(benchController, fileWriter);
        }
    }
}
