/*
 * Copyright (C) 2021 OnGres, Inc.
 * SPDX-License-Identifier: GPL-3.0-only
 */


package com.ongres.labs.pglikedy.it;


public interface Environment {
    String SUPERUSER_PASSWORD = "P0stgr3s";
    String SHARD_1 = "shard1";
    String SHARD_2 = "shard2";
    String SHARD_3 = "shard3";
    String GENERATED_FILES_DIRECTORY = "/tmp/config";
    String GENERATED_SHARDS_CONFIG_FILE = GENERATED_FILES_DIRECTORY + "/application.yaml";
}
