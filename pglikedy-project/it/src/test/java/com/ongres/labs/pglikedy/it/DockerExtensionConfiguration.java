/*
 * Copyright (C) 2021 OnGres, Inc.
 * SPDX-License-Identifier: GPL-3.0-only
 */


package com.ongres.labs.pglikedy.it;


import com.ongres.junit.docker.*;

import static com.ongres.labs.pglikedy.common.config.HardcodedConfig.POSTGRES_PORT;


@DockerContainer(
        image = "postgres:13"
        ,ports = { @Port(internal = POSTGRES_PORT) }
        ,environments = {
                @com.ongres.junit.docker.Environment(key = "POSTGRES_PASSWORD", value = Environment.SUPERUSER_PASSWORD)
        }
        ,mounts = {
                @Mount(
                        reference = DockerExtensionConfiguration.class,
                        path = "/docker-entrypoint-initdb.d",
                        value = "/postgres/init-user-db.sh"
                ),
                @Mount(
                        reference = DockerExtensionConfiguration.class,
                        path = "/etc/postgresql",
                        value = "/postgres/postgresql.conf"
                )
        }
        ,arguments = {"-c", "config_file=/etc/postgresql/postgresql.conf"}
        ,waitFor = @WaitFor("[1] LOG:  database system is ready to accept connections")
)
public interface DockerExtensionConfiguration {}