/*
 * Copyright (C) 2021 OnGres, Inc.
 * SPDX-License-Identifier: GPL-3.0-only
 */


package com.ongres.labs.pglikedy.it;


import com.ongres.junit.docker.DockerContainer;
import com.ongres.junit.docker.DockerExtension;
import com.ongres.junit.docker.WhenReuse;
import org.junit.jupiter.api.Test;


@DockerExtension({
        @DockerContainer(
                extendedBy = DockerExtensionConfiguration.class
                ,alias = Environment.SHARD_1
                ,whenReuse = WhenReuse.EACH_CLASS
        ),
        @DockerContainer(
                extendedBy = DockerExtensionConfiguration.class
                ,alias = Environment.SHARD_2
                ,whenReuse = WhenReuse.EACH_CLASS
        ),
        @DockerContainer(
                extendedBy = DockerExtensionConfiguration.class
                ,alias = Environment.SHARD_3
                ,whenReuse = WhenReuse.EACH_CLASS
        )
})
public class TearDownEnvironmentTest {
    @Test
    public void tearDown() {}
}
